#!/bin/sh -xe


# stable with an non-changing tag
#./_build_rust.sh "1.29.2" "1.1.1" "11.0" "stable-1.29.2"
#./_build_rust.sh "1.30.0" "1.1.1" "11.0" "stable-1.30.0"
#./_build_rust.sh "1.31.1" "1.1.1a" "11.1" "stable-1.31.1"
#./_build_rust.sh "1.32.0" "1.1.1a" "11.1" "stable-1.32.0"

#				 rust, 	  openssl,postgres, tag name
./_build_rust.sh "nightly-2019-08-05" "$LATEST_OPENSSL" "$LATEST_POSTGRES" "nightly-1.38.0"

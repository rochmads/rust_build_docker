# rust_musl_docker – A well-documented container for building Rust crates for MUSL target. Supports dependencies for OpenSSL and Postgres.

This docker image is primarily meant for building statically Rust crates that use **Diesel** and **Rocket** libraries.
A combination of static linking, native (C) dependencies and crates that use heavily compiler plugins is hard to get to
compile. (1) This Docker image is meant to help with that. Not only it supports Diesel and Rocket (and many other crates)
directly, it also is fully commented to help for possible customisation needs. There exists other similar images too,
but the lack of comments make them unhelpful if they don't happen to contain the exact things you need.

`BaseDockerfile.template` and `RustDockerfile.template` are fully commented! Please read them for details.

New images are automatically built daily and stored in this repository. Here's some useful tags:

```
registry.gitlab.com/rust_musl_docker/image:stable-latest              # An image with the newest stable build
registry.gitlab.com/rust_musl_docker/image:beta-latest                # The newest beta
registry.gitlab.com/rust_musl_docker/image:stable-1.30.0              # Rust stable releases with the version number
registry.gitlab.com/rust_musl_docker/image:nightly-2018-10-27         # Rust nightly releases with the date
registry.gitlab.com/rust_musl_docker/image:nightly-2018-10-27-legacy  # Rust nightly releases with the date and older versions of OpenSSL and PostgreSQL
```

## USAGE:

Run this container (you can do that directly but I recommend writing a build script) from your project dir.

Mount the current work dir (`-v $PWD:/workdir`) and Cargo cache dirs (`-v ~/.cargo/git:/root/.cargo/git` &
`-v ~/.cargo/registry:/root/.cargo/registry`) to be able to reuse the cache.

You can customise the build command; in the example below, it builds the release build super-verbosely for musl target.


```
docker run -it --rm \
    -v $PWD:/workdir \
    -v ~/.cargo/git:/root/.cargo/git \
    -v ~/.cargo/registry:/root/.cargo/registry \
    registry.gitlab.com/rust_musl_docker/image:stable-latest \
    cargo build --release -vv --target=x86_64-unknown-linux-musl
```

(1) This has gotten even harder as of late, with Debian Stretch moving to position-independent-code by default and the Rust compiler enabling RELRO. Fortunately, after a lot of trial and error, the build enviroment works.

## Build

If you have no base with specific OpenSSL and Postgres version, then you needs to build base first.

```
$ ./build_base.sh [OPENSSL_VERSION] [POSTGRES_VERSION]
```

Example:

```
$ ./build_base.sh 1.1.1b 9.6.0
```

Build image for Rust toolchain from base:

```
$ export LATEST_OPENSSL=1.1.1b
$ export LATEST_POSTGRES=9.6.0
$ ./build_manual.sh
```


